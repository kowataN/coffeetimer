using System;

public static class TimeFormatter
{
    public static string GetTimeString(int time)
    {
        return new TimeSpan(0, 0, time).ToString(@"mm\:ss");
    }
}
