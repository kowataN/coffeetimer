using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    [SerializeField] AudioSource _audioSource = null;
    [SerializeField] List<AudioClip> _audioClips = null;

    public static SoundManager Inst;

    private void Awake()
    {
        Inst = this;
    }

    public void Play(string name)
    {
        var file = _audioClips.Find(x => x.name == name);
        if (file == null)
        {
            return;
        }

        _audioSource.clip = file;
        _audioSource.Play();
    }
}
