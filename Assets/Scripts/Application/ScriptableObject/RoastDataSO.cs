using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class RoastDetail
{
    /// <summary>
    /// 焙煎
    /// </summary>
    public RoastType Roast;
    /// <summary>
    /// 名前
    /// </summary>
    public string Name;
}

[CreateAssetMenu(menuName = "SO/RoastData")]
public class RoastDataSO : ScriptableObject
{
    [Header("焙煎一覧")]
    public List<RoastDetail> RoastList;
}
