using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GrindDetail
{
    /// <summary>
    /// 挽目
    /// </summary>
    public GrindType Grind;
    /// <summary>
    /// 名前
    /// </summary>
    public string Name;
}

[CreateAssetMenu(menuName = "SO/GrindData")]
public class GrindDataSO : ScriptableObject
{
    [Header("挽目一覧")]
    public List<GrindDetail> GrindList;

}
