using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="SO/RecipeData")]
public class RecipeDataSO : ScriptableObject
{
    [Header("焙煎")]
    public RoastType Roast = RoastType.Medium;

    [Header("挽目")]
    public GrindType Grind = GrindType.Medium;

    [Header("豆の量")]
    public int Weight = 13;

    [Header("抽出量")]
    public int Quantity = 220;

    [Header("湯温")]
    public int Temperature = 90;

    [Header("詳細")]
    public List<Domain.RecipeDetail> Details;
}
