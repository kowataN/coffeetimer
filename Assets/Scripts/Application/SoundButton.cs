using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class SoundButton : MonoBehaviour, IPointerClickHandler
{
    Button _button = null;

    [SerializeField] string _seFile = null;

    private void Awake()
    {
        _button = GetComponent<Button>();
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (_button.enabled)
        {
            SoundManager.Inst.Play(_seFile);
        }
    }
}
