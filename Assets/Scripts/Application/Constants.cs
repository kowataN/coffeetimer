public enum TimerState
{
    Start,
    Pause
}

public static class Constants
{
    public static readonly string SAVEDATA_RECIPE_DATA = "SAVEDATA_RECIPE_DATA";
    public static readonly string PATH_DEFAULT_RECIPE_DATA = "SO/DefaultRecipeData";
    public static readonly string PATH_DEFAULT_ROAST_DATA = "SO/RoastData";
    public static readonly string PATH_DEFAULT_GRIND_DATA = "SO/GrindData";
}

/// <summary>
/// 焙煎度
/// </summary>
[System.Serializable]
public enum RoastType
{
    /// <summary>
    /// ライトロースト
    /// </summary>
    Light,
    /// <summary>
    /// シナモンロースト
    /// </summary>
    Cinnamon,
    /// <summary>
    /// ミディアムロースト
    /// </summary>
    Medium,
    /// <summary>
    /// ハイロースト
    /// </summary>
    High,
    /// <summary>
    /// シティロースト
    /// </summary>
    City,
    /// <summary>
    /// フルシティロースト
    /// </summary>
    FullCity,
    /// <summary>
    /// フレンチロースト
    /// </summary>
    French,
    /// <summary>
    /// イタリアンロースト
    /// </summary>
    Italian,

    Max
}

[System.Serializable]
public enum RoastSimple
{
    /// <summary>
    /// 浅煎り
    /// </summary>
    Light,
    /// <summary>
    /// 中煎り
    /// </summary>
    Medium,
    /// <summary>
    /// 中深煎り
    /// </summary>
    MediumDeep,
    /// <summary>
    /// 深煎り
    /// </summary>
    Deep,

    Max
}

/// <summary>
/// 挽目
/// </summary>
[System.Serializable] 
public enum GrindType
{
    /// <summary>
    /// 粗挽き
    /// </summary>
    Coarse,
    /// <summary>
    /// 中粗挽き
    /// </summary>
    MediumCoarse,
    /// <summary>
    /// 中挽き
    /// </summary>
    Medium,
    /// <summary>
    /// 中細挽き
    /// </summary>
    MediumFine,
    /// <summary>
    /// 細挽き
    /// </summary>
    Fine,
    /// <summary>
    /// 極細挽き
    /// </summary>
    Turkish,

    Max
}