using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using Zenject;

public class AppMainInstaller : MonoInstaller<AppMainInstaller>
{
    public override void InstallBindings()
    {
        Debug.Log($"{this.GetType().Name}::{MethodBase.GetCurrentMethod().Name}");

        Container.BindInterfacesTo<Presentation.TimerView>()
            .FromComponentInHierarchy().AsTransient();
        Container.BindInterfacesTo<Presentation.RecipeView>()
            .FromComponentInHierarchy().AsTransient();
        Container.BindInterfacesTo<Presentation.EditView>()
            .FromComponentInHierarchy().AsTransient();
        Container.BindInterfacesTo<Presentation.AppMainView>()
            .FromComponentInHierarchy().AsTransient();

        Container.BindInterfacesTo<Data.RoastDataStore>().AsSingle();
        Container.BindInterfacesTo<Data.GrindDataStore>().AsSingle();
        Container.BindInterfacesTo<Data.RecipeDataStore>().AsSingle();

        Container.BindInterfacesTo<Domain.TimerModel>().AsSingle();

        Container.BindInterfacesTo<Domain.RoastRepository>().AsSingle();
        Container.BindInterfacesTo<Domain.GrindRepository>().AsSingle();
        Container.BindInterfacesTo<Domain.RecipeRepository>().AsSingle();

        Container.BindInterfacesTo<Domain.EditUseCase>().AsSingle();
        Container.BindInterfacesTo<Domain.TimerUseCase>().AsSingle();
        Container.BindInterfacesTo<Domain.RecipeUseCase>().AsSingle();

        Container.BindInterfacesTo<Presentation.TimerViewModel>().AsSingle();
        Container.BindInterfacesTo<Presentation.RecipeViewModel>().AsSingle();

        Container.BindInterfacesTo<Presentation.EditPresenter>().AsSingle();
        Container.BindInterfacesTo<Presentation.TimerPresenter>().AsSingle();
        Container.BindInterfacesTo<Presentation.RecipePresenter>().AsSingle();
        Container.BindInterfacesTo<Presentation.AppMainPresenter>().AsSingle();
    }
}
