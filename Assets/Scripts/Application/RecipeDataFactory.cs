using UnityEngine;

public static class RecipeDataFactory
{
    public static Domain.RecipeModel CreateDefaultRecipe()
    {
        var data = Resources.Load<RecipeDataSO>(Constants.PATH_DEFAULT_RECIPE_DATA);
        if (data != null)
        {
            var result = new Domain.RecipeModel(
                roast: data.Roast,
                grind: data.Grind,
                weight: data.Weight,
                quantity: data.Quantity,
                temperature: data.Temperature);
            result.SetDetails(data.Details);
            return result;
        }

        return new Domain.RecipeModel(
            roast: RoastType.Medium,
            grind: GrindType.Medium,
            weight: 0,
            quantity: 0,
            temperature: 0);
    }
}
