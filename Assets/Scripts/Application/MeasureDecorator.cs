public abstract class MeasureDecorator
{
    public abstract string GetText(string text);
}

public sealed class WeightMeasure : MeasureDecorator
{
    public override string GetText(string text)
    {
        return text + " g";
    }
}

public sealed class QuantityMeasure : MeasureDecorator
{
    public override string GetText(string text)
    {
        return text + " ml";
    }
}

public sealed class TemperatureMeasure : MeasureDecorator
{
    public override string GetText(string text)
    {
        return text + " 度";
    }
}
