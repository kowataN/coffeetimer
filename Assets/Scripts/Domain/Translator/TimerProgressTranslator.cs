using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace Domain
{
    public interface ITimerProgressTranslator
    {
        public float GetTimerProgress(int crtTime);
    }

    public sealed class TimerProgressTranslator : ITimerProgressTranslator
    {
        Domain.RecipeModel _recipeData = null;
        List<int> _timeTbl = null;

        public TimerProgressTranslator(Domain.RecipeModel receipeData)
        {
            _recipeData = receipeData;
            _timeTbl = new List<int>();
            receipeData.Details.ForEach(x => _timeTbl.Add(x.Time));
        }

        public float GetTimerProgress(int crtTime)
        {
            float ret = 0f;

            return ret;
        }
    }
}