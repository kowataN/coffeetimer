using Zenject;

namespace Domain
{
    public interface IRecipeUseCase
    {
        public RecipeModel GetRecipe();

        public string GetRoastNameByType(RoastType roast);

        public string GetGrindNameByType(GrindType grind);
    }

    public class RecipeUseCase : IRecipeUseCase
    {
        IRecipeRepository _recipeRepository = null;
        IRoastRepository _roastRepository = null;
        IGrindRepository _grindRepository = null;

        public RecipeUseCase(
            IRecipeRepository recipeRepos,
            IRoastRepository roastRepos,
            IGrindRepository grindRepos
            )
        {
            _recipeRepository = recipeRepos;
            _roastRepository = roastRepos;
            _grindRepository = grindRepos;
        }
       
        public RecipeModel GetRecipe() => _recipeRepository.GetRecipe();

        public string GetRoastNameByType(RoastType roast) => _roastRepository.GetNameByRoast(roast);

        public string GetGrindNameByType(GrindType grind) => _grindRepository.GetNameByType(grind);
    }
}
