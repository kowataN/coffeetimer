using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;

namespace Domain
{
    public interface IEditUseCase
    {
        public IReadOnlyList<RoastDetail> GetRoastList();
        public IReadOnlyList<GrindDetail> GetGrindList();
    }

    public class EditUseCase : IEditUseCase
    {
        Data.IRoastDataStore _roastDataStore = null;
        Data.IGrindDataStore _grindDataStore = null;

        public EditUseCase(
            Data.IRoastDataStore roastDataStore,
            Data.IGrindDataStore grindDataStore)
        {
            Debug.Log($"{this.GetType().Name}::{MethodBase.GetCurrentMethod().Name}");

            _roastDataStore = roastDataStore;
            _grindDataStore = grindDataStore;
        }

        public IReadOnlyList<RoastDetail> GetRoastList() => _roastDataStore.DataList;

        public IReadOnlyList<GrindDetail> GetGrindList() => _grindDataStore.DataList;
    }
}
