using System;
using System.Reflection;
using UniRx;
using UnityEngine;

namespace Domain
{
    public interface ITimerUseCase
    {
        public void StartOrStopTimer();
        public void ResetTimer();
        public bool IsTimerStart { get; }
    }

    public class TimerUseCase : ITimerUseCase
    {
        TimerModel _model = null;
        public int CrtTimer => _model.CrtTimer;

        public TimerUseCase()
        {
            //Debug.Log($"{this.GetType().Name}::{MethodBase.GetCurrentMethod().Name}");

            _model = new TimerModel(0);
        }

        public void StartOrStopTimer()
        {
            if (IsTimerStart)
            {
                _model.PauseTimer();
            }
            else
            {
                _model.StartTimer();
            }
        }

        public void ResetTimer() => _model.ResetTimer();

        public bool IsTimerStart => _model.IsCrtState(TimerState.Start);
    }
}
