using Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Domain
{
    public interface IRecipeRepository
    {
        void SetRecipe(Domain.RecipeModel model);
        Domain.RecipeModel GetRecipe();
    }

    public sealed class RecipeRepository : IRecipeRepository
    {
        readonly Data.IRecipeDataStore _dataStore = null;

        public RecipeRepository(Data.IRecipeDataStore dataStore) => _dataStore = dataStore;

        public void SetRecipe(Domain.RecipeModel model) => _dataStore.SetRecipe(model);

        public Domain.RecipeModel GetRecipe() => _dataStore.GetRecipe();
    }
}