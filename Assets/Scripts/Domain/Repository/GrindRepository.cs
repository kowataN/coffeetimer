using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Domain
{
    public interface IGrindRepository
    {
        public string GetNameByType(GrindType grind);
    }

    public class GrindRepository : IGrindRepository
    {
        Data.IGrindDataStore _dataStore = null;

        public GrindRepository(Data.IGrindDataStore dataStore) => _dataStore = dataStore;

        public string GetNameByType(GrindType grind) => _dataStore.GetGrindByType(grind).Name;
    }
}
