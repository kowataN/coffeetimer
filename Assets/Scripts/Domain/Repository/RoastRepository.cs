using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Domain
{
    public interface IRoastRepository
    {
        string GetNameByRoast(RoastType roast);
    }

    public class RoastRepository : IRoastRepository
    {
        Data.IRoastDataStore _dataStore = null;

        public RoastRepository(Data.IRoastDataStore dataStore) => _dataStore = dataStore;

        public string GetNameByRoast(RoastType roast) => _dataStore.GetRoastByRoastType(roast).Name;
    }
}