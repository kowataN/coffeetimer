using System;
using UniRx;

namespace Domain
{
    public interface ITimerModel
    {
        public int CrtTimer { get;  }

        public void AddTimer(int value);
        public void StartTimer();
        public void PauseTimer();
        public void ResetTimer();
        public bool IsCrtState(TimerState state);
    }

    public class TimerModel : ITimerModel
    {
        int _crtTimer;
        public int CrtTimer => _crtTimer;

        TimerState _timerState;

        public TimerModel(int value)
        {
            _crtTimer = value;
            _timerState = TimerState.Pause;
        }

        public void AddTimer(int value) => _crtTimer += value;

        public void StartTimer() => _timerState = TimerState.Start;

        public void PauseTimer() => _timerState = TimerState.Pause;
        
        public void ResetTimer()
        {
            _timerState = TimerState.Pause;
            _crtTimer = 0;
        }

        public bool IsCrtState(TimerState state) => _timerState == state;
    }
}
