using System.Collections.Generic;
using UnityEngine;

namespace Domain
{
    [System.Serializable]
    public class RecipeDetail
    {
        [SerializeField] int _time;
        [SerializeField] string _description;

        /// <summary>
        /// 時間
        /// </summary>
        public int Time => _time;

        /// <summary>
        /// 説明
        /// </summary>
        public string Description => _description;
    }

    [System.Serializable]
    public class RecipeModel
    {
        [SerializeField] RoastType _roast;
        [SerializeField] GrindType _grind;
        [SerializeField] int _weight;
        [SerializeField] int _quantity;
        [SerializeField] int _temperature;
        [SerializeField] List<RecipeDetail> _details;

        public RecipeModel(
            RoastType roast,
            GrindType grind,
            int weight,
            int quantity,
            int temperature)
        {
            _roast = roast;
            _grind = grind;
            _weight = weight;
            _quantity = quantity;
            _temperature = temperature;
            _details = new List<RecipeDetail>();
        }

        /// <summary>
        /// 焙煎
        /// </summary>
        public RoastType Roast => _roast;

        /// <summary>
        /// 挽目
        /// </summary>
        public GrindType Grind => _grind;

        /// <summary>
        /// 豆の量
        /// </summary>
        public int Weight => _weight;

        /// <summary>
        /// 湯量
        /// </summary>
        public int Quantity => _quantity;

        /// <summary>
        /// 温度
        /// </summary>
        public int Temperature => _temperature;

        public List<RecipeDetail> Details => _details;

        public void SetDetails(List<RecipeDetail> details) => _details = details;
    }

    [System.Serializable]
    public class RecipeListModel
    {
    }
}
