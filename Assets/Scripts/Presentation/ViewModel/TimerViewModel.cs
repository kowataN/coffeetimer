using UniRx;

namespace Presentation
{
    public interface ITimerViewModel
    {
        public IReadOnlyReactiveProperty<int> CurrentTimer { get; }
        public void AddCurrentTimer(int time);
        public void SetCurrentTimer(int time);
        public string GetCurrentTimer();
        public float GetTimerProgress();
    }

    public class TimerViewModel : ITimerViewModel
    {
        ReactiveProperty<int> _currentTimer;
        Domain.TimerProgressTranslator _translator = null;

        public TimerViewModel() =>_currentTimer = new ReactiveProperty<int>(0);

        public IReadOnlyReactiveProperty<int> CurrentTimer => _currentTimer;

        public void AddCurrentTimer(int time) =>_currentTimer.Value += time;

        public void SetCurrentTimer(int time) =>_currentTimer.Value = time;

        public string GetCurrentTimer() => TimeFormatter.GetTimeString(_currentTimer.Value);

        public float GetTimerProgress() => 1.0f;
    }
}
