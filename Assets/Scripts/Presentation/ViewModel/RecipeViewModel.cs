using System.Collections.Generic;
using UniRx;
using Zenject;

namespace Presentation
{
    public interface IRecipeViewModel
    {
        public string Roast { get; }
        public string Grind { get; }
        public int Weight { get; }
        public int Quantity { get; }
        public int Temperature { get; }
        public List<Domain.RecipeDetail> Details { get; }
    }

    public sealed class RecipeViewModel : IRecipeViewModel
    {
        string _roast;
        string _grind;
        int _weight;
        int _quantity;
        int _temperature;
        List<Domain.RecipeDetail> _details = null;

        public string Roast => _roast;
        public string Grind => _grind;
        public int Weight => _weight;
        public int Quantity => _quantity;
        public int Temperature => _temperature;
        public List<Domain.RecipeDetail> Details => _details;

        public RecipeViewModel(
            Domain.RecipeModel recipe,
            string roast,
            string grind)
        {
            _roast = roast;
            _grind = grind;
            _weight = recipe.Weight;
            _quantity = recipe.Quantity;
            _temperature = recipe.Temperature;

            _details = new List<Domain.RecipeDetail>();
            recipe.Details.ForEach(x => _details.Add(x));
        }
    }
}
