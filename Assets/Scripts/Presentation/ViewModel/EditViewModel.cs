using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Presentation
{
    public class EditViewModel
    {
        IReadOnlyList<string> _roastList;
        IReadOnlyList<string> _grindList;

        public EditViewModel(
            IReadOnlyList<RoastDetail> roastList,
            IReadOnlyList<GrindDetail> grindList)
        {
            _roastList = roastList
                .Select(detail => detail.Name)
                .ToList();

            _grindList = grindList
                .Select(detail => detail.Name)
                .ToList();
        }

        public IReadOnlyList<string> RoastList => _roastList;
        public IReadOnlyList<string> GrindList => _grindList;
    }
}