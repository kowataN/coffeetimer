using System.Reflection;
using UniRx;
using UnityEngine;
using Zenject;

namespace Presentation
{
    public interface IEditPresenter
    {
        public void SetActive(bool active);
    }

    public class EditPresenter : IEditPresenter, IInitializable
    {
        IEditView _view = null;
        Domain.IEditUseCase _useCase = null;
        EditViewModel _viewModel = null;

        public EditPresenter(
            IEditView view,
            Domain.IEditUseCase useCase
            )
        {
            _view = view;
            _useCase = useCase;
        }

        public void Initialize() => Setup();

        void Setup()
        {
            Debug.Log($"{this.GetType().Name}::{MethodBase.GetCurrentMethod().Name}");

            _viewModel = new EditViewModel(
                roastList: _useCase.GetRoastList(),
                grindList: _useCase.GetGrindList());

            _view.OnClickCloseButton
                .Subscribe(_ => _view.SetActive(false))
                .AddTo(_view.GetGameObject);

            _view.RoastList = _viewModel.RoastList;
            _view.GrindList = _viewModel.GrindList;

            // 一旦非表示にする
            SetActive(false);
        }

        public void SetActive(bool active) => _view.SetActive(active);
    }
}