using System.Reflection;
using UniRx;
using UnityEngine;
using UnityEngine.EventSystems;
using Zenject;

namespace Presentation
{
    public interface IRecipePresenter
    {
    }

    public class RecipePresenter : IRecipePresenter, IInitializable
    {
        Domain.IRecipeUseCase _useCase = null;
        IRecipeView _view = null;
        ITimerViewModel _timerViewModel = null;
        IRecipeViewModel _viewModel = null;
        
        MeasureDecorator _weightMeasure = new WeightMeasure();
        MeasureDecorator _uantityMeasure = new QuantityMeasure();
        MeasureDecorator _temperatureMeasure = new TemperatureMeasure();

        public RecipePresenter(
            Domain.IRecipeUseCase useCase,
            IRecipeView view,
            ITimerViewModel timerViewModel
            )
        {
            _useCase = useCase;
            _view = view;
            _timerViewModel = timerViewModel;
        }

        public void Initialize() => Setup();

        void Setup()
        {
            var recipe = _useCase.GetRecipe();
            _viewModel = new RecipeViewModel(
                recipe: recipe,
                roast: _useCase.GetRoastNameByType(recipe.Roast),
                grind: _useCase.GetGrindNameByType(recipe.Grind)
                );

            SetRoast(_viewModel.Roast);
            SetGrind(_viewModel.Grind);
            SetWeight(_viewModel.Weight);
            SetQuantity(_viewModel.Quantity);
            SetTemperature(_viewModel.Temperature);

            _view.SetDetails(_viewModel.Details);

            _timerViewModel.CurrentTimer
                .ObserveEveryValueChanged(x => x.Value)
                .Subscribe(x => _view.SetSelected(x))
                .AddTo(_view.GetGameObject);
        }

        void SetRoast(string roast) => _view.Roast.text = roast;
        void SetGrind(string grind) => _view.Grind.text = grind;
        void SetWeight(int value) => _view.Weight.text = _weightMeasure.GetText(value.ToString());
        void SetQuantity(int value) => _view.Quantity.text = _uantityMeasure.GetText(value.ToString());
        void SetTemperature(int value) => _view.Temperature.text = _temperatureMeasure.GetText(value.ToString());
    }
}