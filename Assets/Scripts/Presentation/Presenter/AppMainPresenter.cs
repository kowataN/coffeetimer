using System.Reflection;
using UniRx;
using UnityEngine;
using UnityEngine.EventSystems;
using Zenject;

namespace Presentation
{
    public interface IAppMainPresenter
    {
    }

    public sealed class AppMainPresenter : IAppMainPresenter, IInitializable
    {
        IAppMainView _appView = null;
        IEditPresenter _editPresenter = null;

        public AppMainPresenter(
            IAppMainView appView,
            IEditPresenter editPresenter
            )
        {
            _appView = appView;
            _editPresenter = editPresenter;
        }

        public void Initialize() => Setup();

        void Setup()
        {
            Debug.Log($"{this.GetType().Name}::{MethodBase.GetCurrentMethod().Name}");

            _appView.EditButtonOnClick
                .Subscribe(_ => OnClickEditButton())
                .AddTo(_appView.GetGameObject);
        }

        void OnClickEditButton() => _editPresenter.SetActive(true);
    }
}