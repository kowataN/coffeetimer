using System;
using System.Reflection;
using UniRx;
using UnityEngine;
using UnityEngine.EventSystems;
using Zenject;

namespace Presentation
{
    public interface ITimerPresenter
    {
        public ITimerViewModel TimerViewModel { get; }
    }

    public class TimerPresenter : ITimerPresenter, IInitializable
    {
        Domain.ITimerUseCase _useCase = null;
        ITimerViewModel _viewModel = null;
        ITimerView _view = null;

        public ITimerViewModel TimerViewModel => _viewModel;

        public TimerPresenter(
            Domain.ITimerUseCase useCase,
            ITimerViewModel viewModel,
            ITimerView view
            )
        {
            _useCase = useCase;
            _viewModel = viewModel;
            _view = view;
        }

        public void Initialize() => Setup();

        void Setup()
        {
            // スタートボタン押下
            _view.OnClickStart
                .Subscribe(_ => OnClickStartButton())
                .AddTo(_view.GetGameObject);

            // リセットボタン押下
            _view.OnClickReset
                .Subscribe(_ => OnClickResetButton())
                .AddTo(_view.GetGameObject);

            Observable.Timer(TimeSpan.FromSeconds(1),
                              TimeSpan.FromSeconds(1))
                .Where(_ => _useCase.IsTimerStart)
                .Subscribe(_ => {
                    _viewModel.AddCurrentTimer(1);
                })
                .AddTo(_view.GetGameObject);

            _viewModel.CurrentTimer
                .ObserveEveryValueChanged(x => x.Value)
                .Subscribe(time => {
                    _view.CurrentTimer = _viewModel.GetCurrentTimer();
                    _view.UpdateProgressImage(_viewModel.GetTimerProgress());
                })
                .AddTo(_view.GetGameObject);
        }

        void OnClickStartButton() => _useCase.StartOrStopTimer();

        void OnClickResetButton()
        {
            _useCase.ResetTimer();
            _viewModel.SetCurrentTimer(0);
        }
    }
}