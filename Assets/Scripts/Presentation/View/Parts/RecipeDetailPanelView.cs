using UnityEngine;
using UnityEngine.UI;

namespace Presentation
{
    public interface IRecipeDetailPanelView
    {
        public bool IsSelected { get; }
        public void SetSelected(bool value);
        public void Setup(int time, string description);
    }

    public sealed class RecipeDetailPanelView : MonoBehaviour, IRecipeDetailPanelView
    {
        [SerializeField] Text _time = null;
        [SerializeField] Text _description = null;
        [SerializeField] Image _selectBG = null;

        public bool IsSelected => _selectBG.enabled;

        public void SetSelected(bool value) => _selectBG.enabled = value;

        public void Setup(int time, string description)
        {
            _time.text = TimeFormatter.GetTimeString(time);
            _description.text = description;
        }
    }
}