using System;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace Presentation
{
    public interface IEditView
    {
        public IObservable<int> OnValueChangedRecipe { get; }
        public IObservable<Unit> OnClickCreateButton { get; }
        public IObservable<Unit> OnClickAddButton { get; }
        public IObservable<Unit> OnClickEditButton { get; }
        public IObservable<Unit> OnClickDeleteButton { get; }
        public IObservable<Unit> OnClickCloseButton { get; }
        public IObservable<Unit> OnClickSaveButton { get; }

        public IReadOnlyList<string> RoastList { set; }
        public IReadOnlyList<string> GrindList { set; }

        public GameObject GetGameObject { get; }
        public void SetActive(bool active);
    }

    public sealed class EditView : MonoBehaviour, IEditView
    {
        IReadOnlyList<string> _roastList;
        IReadOnlyList<string> _grindList;

        #region SerializeField
        [SerializeField] Dropdown _dropdownRecipe = null;
        [SerializeField] Button _buttonCreate = null;
        [SerializeField] Dropdown _dropdownRoast = null;
        [SerializeField] Dropdown _dropdownGrind = null;
        [SerializeField] InputField _inputWeight = null;
        [SerializeField] InputField _inputQuantity = null;
        [SerializeField] InputField _inputTemperature = null;
        [SerializeField] ScrollRect _scrollView = null;
        [SerializeField] Button _buttonAdd = null;
        [SerializeField] Button _buttonEdit = null;
        [SerializeField] Button _buttonDelete = null;
        [SerializeField] Button _buttonClose = null;
        [SerializeField] Button _buttonSave = null;
        #endregion

        public IObservable<int> OnValueChangedRecipe => _dropdownRecipe.OnValueChangedAsObservable();
        public IObservable<Unit> OnClickCreateButton => _buttonCreate.OnClickAsObservable();
        public IObservable<Unit> OnClickAddButton => _buttonAdd.OnClickAsObservable();
        public IObservable<Unit> OnClickEditButton => _buttonEdit.OnClickAsObservable();
        public IObservable<Unit> OnClickDeleteButton => _buttonDelete.OnClickAsObservable();
        public IObservable<Unit> OnClickCloseButton => _buttonClose.OnClickAsObservable();
        public IObservable<Unit> OnClickSaveButton => _buttonSave.OnClickAsObservable();

        public IReadOnlyList<string> RoastList
        {
            set
            {
                _roastList = value;

                var options = _roastList
                    .Select(roastName => new Dropdown.OptionData(roastName))
                    .ToList();
                _dropdownRoast.options = options;
            }
        }

        public IReadOnlyList<string> GrindList
        {
            set
            {
                _grindList = value;

                var options = _grindList
                    .Select(grindName => new Dropdown.OptionData(grindName))
                    .ToList();
                _dropdownGrind.options = options;
            }
        }

        public GameObject GetGameObject => this.gameObject;

        public void SetActive(bool active) => this.gameObject.SetActive(active);

    }
}