using System;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Presentation
{
    public interface ITimerView
    {
        public float TimerImage { set; }
        public string CurrentTimer { set; }
        public IObservable<Unit> OnClickStart { get; }
        public IObservable<Unit> OnClickReset { get; }
        public GameObject GetGameObject { get; }

        public void UpdateProgressImage(float value);
    }

    public sealed class TimerView : MonoBehaviour, ITimerView
    {
        [SerializeField] Image _timerImage = null;
        [SerializeField] Text _currentTimer = null;
        [SerializeField] Button _startButton = null;
        [SerializeField] Button _resetButton = null;

        public float TimerImage
        {
            set => _timerImage.fillAmount = value;
        }

        public string CurrentTimer
        {
            set => _currentTimer.text = value;
        }

        public IObservable<Unit> OnClickStart => _startButton.OnClickAsObservable();

        public IObservable<Unit> OnClickReset => _resetButton.OnClickAsObservable();

        public GameObject GetGameObject => this.gameObject;

        public void UpdateProgressImage(float value) => _timerImage.fillAmount = value;
    }
}