using System;
using System.Reflection;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Presentation
{
    public interface IAppMainView
    {
        public GameObject GetGameObject { get; }
        public IObservable<Unit> EditButtonOnClick { get; }
    }

    public sealed class AppMainView : MonoBehaviour, IAppMainView, IInitializable
    {
        [SerializeField] Button _editButton = null;

        public GameObject GetGameObject => this.gameObject;

        public IObservable<Unit> EditButtonOnClick => _editButton.OnClickAsObservable();

        public void Initialize() => Setup();

        private void Setup()
        {
            Debug.Log($"{this.GetType().Name}::{MethodBase.GetCurrentMethod().Name}");
        }
    }
}