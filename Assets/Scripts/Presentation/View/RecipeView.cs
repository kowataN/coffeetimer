using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Presentation
{
    public interface IRecipeView
    {
        public Text Roast { get; }
        public Text Grind { get; }
        public Text Weight { get; }
        public Text Quantity { get; }
        public Text Temperature { get; }
        public GameObject GetGameObject { get; }

        public void SetDetails(List<Domain.RecipeDetail> details);
        public void SetSelected(int time);
    }

    public sealed class RecipeView : MonoBehaviour, IRecipeView
    {
        [SerializeField] GameObject _panelPrefab = null;
        [SerializeField] Text _roast = null;
        [SerializeField] Text _grind = null;
        [SerializeField] Text _weight = null;
        [SerializeField] Text _quantity = null;
        [SerializeField] Text _temperature = null;
        [SerializeField] GameObject _scrollContent = null;
        [SerializeField] string _seFile = null;

        public Text Roast => _roast;
        public Text Grind => _grind;
        public Text Weight => _weight;
        public Text Quantity => _quantity;
        public Text Temperature => _temperature;
        public GameObject GetGameObject => this.gameObject;

        IRecipeDetailPanelView _crtPanel = null;

        // TODO: これを何処かに移す
        Dictionary<int, IRecipeDetailPanelView> _timeTbl = new Dictionary<int, IRecipeDetailPanelView>();

        public void SetDetails(List<Domain.RecipeDetail> details)
        {
            foreach (Transform obj in _scrollContent.transform)
            {
                GameObject.Destroy(obj.gameObject);
            }

            _timeTbl.Clear();
            details.ForEach(x => 
            {
                var obj = Instantiate(_panelPrefab, _scrollContent.transform);
                var script = obj.GetComponentInChildren<IRecipeDetailPanelView>();
                if (script != null)
                {
                    script.Setup(
                        time: x.Time,
                        description: x.Description);

                    if (!_timeTbl.ContainsKey(x.Time))
                    {
                        _timeTbl.Add(x.Time, script);
                    }
                }
            });
        }

        public void SetSelected(int time)
        {
            IRecipeDetailPanelView outView = null;
            _timeTbl.TryGetValue(time, out outView);
            if (outView != null)
            {
                _crtPanel?.SetSelected(false);
                _crtPanel = outView;
                _crtPanel.SetSelected(true);

                if (time != 0)
                {
                    SoundManager.Inst.Play(_seFile);
                }
            }
        }
    }
}