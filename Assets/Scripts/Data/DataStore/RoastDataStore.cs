using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace Data
{
    public interface IRoastDataStore
    {

        public IReadOnlyList<RoastDetail> DataList { get; }

        public RoastDetail GetRoastByRoastType(RoastType roast);
    }

    public class RoastDataStore : IRoastDataStore
    {
        List<RoastDetail> _dataList = new List<RoastDetail>();

        public RoastDataStore()
        {
            var data = Resources.Load<RoastDataSO>(Constants.PATH_DEFAULT_ROAST_DATA);
            if (data != null)
            {
                _dataList.Capacity = data.RoastList.Count;
                _dataList = data.RoastList;
            }
        }

        public IReadOnlyList<RoastDetail> DataList => _dataList;

        public RoastDetail GetRoastByRoastType(RoastType roast) => _dataList.FirstOrDefault(x => x.Roast == roast);
    }
}