using Data;
using UnityEngine;

namespace Data
{
    public interface IRecipeDataStore
    {
        public void SetRecipe(Domain.RecipeModel entity);
        public Domain.RecipeModel GetRecipe();
    }

    public class RecipeDataStore : IRecipeDataStore
    {
        public void SetRecipe(Domain.RecipeModel model)
        {
            string json = JsonUtility.ToJson(model);
            PlayerPrefs.SetString(Constants.SAVEDATA_RECIPE_DATA, json);
        }

        public Domain.RecipeModel GetRecipe()
        {
            if (PlayerPrefs.HasKey(Constants.SAVEDATA_RECIPE_DATA))
            {
                string json = PlayerPrefs.GetString(Constants.SAVEDATA_RECIPE_DATA);
                var data = JsonUtility.FromJson<Domain.RecipeModel>(json);
                if (data != null)
                {
                    return data;
                }
            }

            // データが存在しないorデータが不正なら新規作成
            Domain.RecipeModel entity = RecipeDataFactory.CreateDefaultRecipe();
            this.SetRecipe(entity);
            return entity;
        }
    }
}