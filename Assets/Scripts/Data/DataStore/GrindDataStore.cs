using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Data
{
    public interface IGrindDataStore
    {
        public IReadOnlyList<GrindDetail> DataList { get; }
        public GrindDetail GetGrindByType(GrindType type);
    }

    public sealed class GrindDataStore : IGrindDataStore
    {
        List<GrindDetail> _dataList = new List<GrindDetail>();

        public GrindDataStore()
        {
            var data = Resources.Load<GrindDataSO>(Constants.PATH_DEFAULT_GRIND_DATA);
            if (data != null)
            {
                _dataList.Capacity = data.GrindList.Count;
                _dataList = data.GrindList;
            }
        }

        public IReadOnlyList<GrindDetail> DataList => _dataList;

        public GrindDetail GetGrindByType(GrindType type) => _dataList.FirstOrDefault(x => x.Grind == type);
    }
}